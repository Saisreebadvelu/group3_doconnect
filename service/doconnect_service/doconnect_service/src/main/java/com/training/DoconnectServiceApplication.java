package com.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoconnectServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DoconnectServiceApplication.class, args);
	}

}
